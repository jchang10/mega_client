// import * as dotenv from 'dotenv';
import * as execa from 'execa';

// dotenv.config();
const DEBUG=true

// introspect Graphql API and save the result to `schema.json`
// using command:
//   > node_modules/.bin/ts-node -O '{"module":"commonjs"}' generate-schema.ts
//#
if (!DEBUG) {
    // https://api.withme.live/graphql
    execa.sync('apollo-codegen', [
    'introspect-schema',
    'https://api.withme.live/graphql',
    '--output',
    'schema.json',
    '--header',
    'Bypass:hack'
    //  '--header',
    //  'Authorization: bearer ' + process.env.GITHUB_TOKEN,
    ]);
} else {
    // http://localhost:5000/graphql
    execa.sync('apollo-codegen', [
        'introspect-schema',
        'http://localhost:5000/graphql',
        '--output',
        'schema.json',
    //  '--header',
    //  'Authorization: bearer ' + process.env.GITHUB_TOKEN,
    ]);
}
console.log('schema.json generated');

// inpsect actual queries in `index.ts` and generate TypeScript types in `schema.ts`
execa.sync('apollo-codegen', [
  'generate',
  'src/**/*.tsx',
  '--schema',
  'schema.json',
  '--target',
  'typescript',
  '--output',
  'src/graphql.ts',
  '--add-typename',
]);
console.log('graphql.ts generated');
