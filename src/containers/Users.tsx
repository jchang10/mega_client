import * as React from 'react';
import { Component } from "react";
import "./Login.css";
import '../config';

import Amplify, {Auth, Storage} from 'aws-amplify';
import aws_exports from '../aws-exports';
  //@ts-ignore
import AWS from '../../node_modules/aws-sdk';

import { Query } from "react-apollo";
import gql from "graphql-tag";

import { RouteProps } from 'react-router'

Amplify.configure(aws_exports);
// AWS.config.logger = console;
// window.LOG_LEVEL = 'DEBUG';

const USERS_QUERY = gql`
query UsersQuery {
  users {
    username email enabled createDate
  }
}
`;

interface User {
  username: string,
  email: string,
  enabled: string,
  createDate: string,
}

interface UsersTableProps {
  users: User[],
  filterText: string
}
class UsersTable extends React.Component<UsersTableProps> {
  render() {
    const {users, filterText} = this.props;
    return (
      <table>
        <tbody>
          <tr>
            <th> Username </th>
            <th> Email </th>
            <th> Enabled </th>
            <th> CreateDate </th>
          </tr>
          { users.map(user => {
              const { filterText } = this.props
              let rows: any[] = [];
              if (filterText != '' &&
                user.username.indexOf(filterText) == -1)
                return
              rows.push(
                <tr key={user.username}>
                  <td> {user.username} </td>
                  <td> {user.email} </td>
                  <td> {user.enabled} </td>
                  <td> {user.createDate} </td>
                </tr>)
              return rows
            })
          }
        </tbody>
      </table>
    );
  }
}

interface SearchBarProps {
  filterText: string,
  onFilterTextChange: any
}
class SearchBar extends React.Component
  <SearchBarProps> 
{
  handleFilterTextChange = (e) => {
    this.props.onFilterTextChange(e.target.value);
  }
  render() {
    return (
      <form>
        <input
          type="text"
          placeholder="Search..."
          value={this.props.filterText}
          onChange={this.handleFilterTextChange}
        />
      </form>
    );
  }
}

interface FilterableUsersTableProps {
  users: any[],
}
interface FilterableUsersTableState {
  filterText: string
}
class FilterableUsersTable extends React.Component
  <FilterableUsersTableProps, FilterableUsersTableState> 
{
    state = {
      filterText:''
    }
  handleFilterTextChange = (filterText) => {
    this.setState({
      filterText: filterText
    });
  }
  render() {
    return (
      <div>
        <SearchBar 
          filterText={this.state.filterText}
          onFilterTextChange={this.handleFilterTextChange}
        />
        <UsersTable 
          users={this.props.users} 
          filterText={this.state.filterText}
        />
      </div>
    );
  }
}

interface Data {
  users: User[],
}

class UsersQuery extends Query<Data|any> {}

const Users = (props:RouteProps) => (
    <UsersQuery query={USERS_QUERY}>
      {({ loading, error, data:{users}}) => {
        if (loading) return "Loading...";
        if (error) return `Error! ${error.message}`;

        return (
          <React.Fragment>
            <div><FilterableUsersTable users={users} /></div>
          </React.Fragment>
        );
      }}
    </UsersQuery>
  )

const MOCK_USERS = [
  {username:'testuser1',email:'test1@email.com',enabled:true},
  {username:'testuser2',email:'test2@email.com',enabled:true},
  {username:'testuser3',email:'test3@email.com',enabled:true}
]

function UsersMock() {
  return (
    <FilterableUsersTable users={MOCK_USERS} />
  );
}

export default Users
