import * as React from "react";
import Amplify, {Auth, Storage} from 'aws-amplify';
import aws_exports from '../aws-exports';
import AWS from '../../node_modules/aws-sdk';

import gql from "graphql-tag";
import { Query } from 'react-apollo';

import AddNote from './AddNote';
import { RouteProps } from "react-router";

Amplify.configure(aws_exports);
// AWS.config.logger = console;
// (window as any).LOG_LEVEL = 'DEBUG';

/*
User
Search bar
NotesTable
  NoteRow
  NoteRow
  NoteRow
*/

type Note = {
  userId: string,
  note1: string,
  note2: string,
  note3: string,
}

interface NotesTableProps {
  notes: Note[],
  filterText: string
}
class NotesTable extends React.Component<NotesTableProps> {
  render() {
    const {notes, filterText} = this.props;
    return (
      <table>
        <tbody>
          <tr>
            <th> Userid </th>
            <th> Key </th>
            <th> Value </th>
          </tr>
          { notes.map(note => {
              const { filterText } = this.props
              let rows: any[] = [];
              if (filterText != '' &&
                note.userId.indexOf(filterText) == -1)
                return
              rows.push(
                <tr key={note.userId}>
                  <td>{note.userId}</td>
                </tr>)
              Object.keys(note).filter(k => k != 'userId')
                .filter(k => k[0] != '_')
                .filter(k => k != 'id')
                .map(k => rows.push(
                  <tr key={k}>
                    <td> </td>
                    <td> {k} </td>
                    <td> {note[k]} </td>
                  </tr>
                ))
              return rows
            })
          }
        </tbody>
      </table>
    );
  }
}

interface SearchBarProps {
  filterText: string,
  onFilterTextChange: any
}
class SearchBar extends React.Component
  <SearchBarProps> 
{
  handleFilterTextChange = (e) => {
    this.props.onFilterTextChange(e.target.value);
  }
  render() {
    return (
      <form>
        <input
          type="text"
          placeholder="Search..."
          value={this.props.filterText}
          onChange={this.handleFilterTextChange}
        />
      </form>
    );
  }
}

interface FilterableNotesTableProps {
  notes: any[],
}
interface FilterableNotesTableState {
  filterText: string
}
class FilterableNotesTable extends React.Component
  <FilterableNotesTableProps, FilterableNotesTableState> 
{
    state = {
      filterText:''
    }
  handleFilterTextChange = (filterText) => {
    this.setState({
      filterText: filterText
    });
  }
  render() {
    return (
      <div>
        <SearchBar 
          filterText={this.state.filterText}
          onFilterTextChange={this.handleFilterTextChange}
        />
        <NotesTable 
          notes={this.props.notes} 
          filterText={this.state.filterText}
        />
      </div>
    );
  }
}

export const NOTES_QUERY = gql`
  query Notes {
    notes {
      id
      userId
      note1
      note2
      note3
    }
  }
`;

interface Data {
  notes: Note[],
}

class NotesQuery extends Query<Data|any> {}

const Notes = (props:RouteProps) => (
  <NotesQuery query={NOTES_QUERY}>
    {({ loading, error, data:{notes}}) => {
      if (loading) return "Loading...";
      if (error) return `Error! ${error.message}`;

      return (
        <React.Fragment>
          <div><AddNote /></div>
          <div><FilterableNotesTable notes={notes} /></div>
        </React.Fragment>
      );
    }}
  </NotesQuery>
)

const MOCK_NOTES = [
  {userId:'jctest101', note1:'note1', note2:'note2', note3:'note3'},
  {userId:'test2', note1:'note4', note2:'note2', note3:'note3'},
  {userId:'test3', note1:'note5', note2:'note2', note3:'note3'}
];

function NotesMock() {
  return (
    <FilterableNotesTable notes={MOCK_NOTES} />
  );
}

export default Notes

