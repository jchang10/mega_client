import * as React from "react";

import gql from "graphql-tag";
import { Query } from 'react-apollo';

import AddNote from './AddNoteAppSync';
import { RouteProps } from "react-router";

// AWS.config.logger = console;
// (window as any).LOG_LEVEL = 'DEBUG';

import { graphqlOperation } from 'aws-amplify'
import { Connect } from 'aws-amplify-react'

// let myAppConfig = {
//   // ...
//   'aws_appsync_graphqlEndpoint': 'https://rmim4jmzzrbqhkmbfupnzbhasm.appsync-api.us-west-2.amazonaws.com/graphql',
//   'aws_appsync_region': 'us-west-2',
//   'aws_appsync_authenticationType': 'AMAZON_COGNITO_USER_POOLS', // You have configured Auth with Amazon Cognito User Pool ID and Web Client Id
//   // ...
// }
// Amplify.configure(myAppConfig);

/*
User
Search bar
NotesTable
  NoteRow
  NoteRow
  NoteRow
*/

type Note = {
  userId: string,
  note1: string,
  note2: string,
  note3: string,
}

interface NotesTableProps {
  notes: Note[],
  filterText: string
}
class NotesTable extends React.Component<NotesTableProps> {
  render() {
    const {notes, filterText} = this.props;
    return (
      <table>
        <tbody>
          <tr>
            <th> Userid </th>
            <th> Key </th>
            <th> Value </th>
          </tr>
          { notes ? (
              notes.map(note => {
                const { filterText } = this.props
                let rows: any[] = [];
                if (filterText != '' &&
                  note.userId.indexOf(filterText) == -1)
                  return
                rows.push(
                  <tr key={note.userId}>
                    <td>{note.userId}</td>
                  </tr>)
                Object.keys(note).filter(k => k != 'userId')
                  .filter(k => k[0] != '_')
                  .filter(k => k != 'id')
                  .map(k => rows.push(
                    <tr key={k}>
                      <td> </td>
                      <td> {k} </td>
                      <td> {note[k]} </td>
                    </tr>
                  ))
                return rows
              })
            ) : (
              "Nothing"
            )
          }
        </tbody>
      </table>
    );
  }
}

interface SearchBarProps {
  filterText: string,
  onFilterTextChange: any
}
class SearchBar extends React.Component
  <SearchBarProps> 
{
  handleFilterTextChange = (e) => {
    this.props.onFilterTextChange(e.target.value);
  }
  render() {
    return (
      <form>
        <input
          type="text"
          placeholder="Search..."
          value={this.props.filterText}
          onChange={this.handleFilterTextChange}
        />
      </form>
    );
  }
}

interface FilterableNotesTableProps {
  notes: any[],
}
interface FilterableNotesTableState {
  filterText: string
}
class FilterableNotesTable extends React.Component
  <FilterableNotesTableProps, FilterableNotesTableState> 
{
    state = {
      filterText:''
    }
  handleFilterTextChange = (filterText) => {
    this.setState({
      filterText: filterText
    });
  }
  render() {
    return (
      <div>
        <SearchBar 
          filterText={this.state.filterText}
          onFilterTextChange={this.handleFilterTextChange}
        />
        <NotesTable 
          notes={this.props.notes} 
          filterText={this.state.filterText}
        />
      </div>
    );
  }
}

export const NOTES_QUERY = gql`
  query NotesQuery {
    notes {
      id
      userId
      note1
      note2
      note3
    }
  }
`;

const QUERY_NOTES = `
  query NotesQuery {
    notes {
      user_id
      note1
      note2
      note3
    }
  }
`

interface Data {
  notes: Note[],
}

class NotesQuery extends Query<Data|any> {}

const Notes = (props:RouteProps) => (
  <Connect query={graphqlOperation(QUERY_NOTES)}>
    {({ loading, error, data}) => {
      if (loading) return "Loading...";
      if (error) return `Error! ${error.message}`;

      const {notes=[]} = data
      return (
        <React.Fragment>
          <div><AddNote /></div>`
          <div><FilterableNotesTable notes={notes} /></div>
        </React.Fragment>
      );
    }}
  </Connect>
)

const MOCK_NOTES = [
  {userId:'jctest101', note1:'note1', note2:'note2', note3:'note3'},
  {userId:'test2', note1:'note4', note2:'note2', note3:'note3'},
  {userId:'test3', note1:'note5', note2:'note2', note3:'note3'}
];

function NotesMock() {
  return (
    <FilterableNotesTable notes={MOCK_NOTES} />
  );
}

export default Notes

