import * as React from 'react';
import { Component } from "react";
import '../../config';

//@ts-ignore
import {CognitoAuth} from 'amazon-cognito-auth-js/dist/amazon-cognito-auth';

var auth: any;

function initCognitoSDK() {
  var callback_signin_url = 'http://localhost:3000/auth/cognito_signin';
  var callback_signout_url = 'http://localhost:3000/auth/cognito_signout';
  var authData = {
/*
   ClientId : '<TODO: your app client ID here>', // Your client id here
   AppWebDomain : '<TODO: your app web domain here>',
   TokenScopesArray : '<TODO: your scope array here>',
   RedirectUriSignIn : '<TODO: your redirect url when signed in here>',
   RedirectUriSignOut : '<TODO: your redirect url when signed out here>'
 */
ClientId : '1so40q18pul5kd52pmm8idr6rn', // Your client id here
AppWebDomain : 'withme-mega-testwebapp.auth.us-west-2.amazoncognito.com',
TokenScopesArray : [],
RedirectUriSignIn : callback_signin_url,
RedirectUriSignOut : callback_signout_url
  };
  auth = new CognitoAuth(authData);
  auth.userhandler = {
/*
   onSuccess: <TODO: your onSuccess callback here>,
   onFailure: <TODO: your onFailure callback here>
 */
onSuccess: function(result: any) {
    var idToken = result.getIdToken().getJwtToken();
    var accToken = result.getAccessToken().getJwtToken();
    alert('success!'+idToken);
    // $.cookie('id_token', idToken, {path:'/'})
    // $.cookie('access_token', accToken, {path:'/'})
    //alert("Sign in success"+accToken);
    //window.open(callback_signin_url3+'?id_token='+idToken+'&access_token='+accToken,"_self")
    /*
 //works using low-level call
 $.ajax({ url: callback_signin_url3,
 headers: { 'Authorization': 'Bearer '+idToken },
 error: function(err) { alert('error'); },
 success: function(data) {
 $("#messages").text("Success")
 }});
     */
    let htmlstr = '<b> Signin Success </b><br/>';
    if (idToken) {
  var payload = idToken.split('.')[1];
  var payload_obj = JSON.parse(atob(payload))
  if (payload_obj.email)
      htmlstr += '<b>Welcome '+payload_obj.email+'</b><br/>\n'
  var formatted = JSON.stringify(atob(payload), null, 4);
  htmlstr += 'idToken:<textarea rows=5 cols=100>'+formatted+'</textarea>';
    }
    if (accToken) {
  var payload = accToken.split('.')[1];
  var formatted = JSON.stringify(atob(payload), null, 4);
  htmlstr += '<br/>\naccessToken:<textarea rows=5 cols=100>'+formatted+'</textarea>';
    }
    // $("#signInMessage").html(htmlstr);
},
onFailure: function(err: any) {
    alert("Error!" + err);
}};

  // The default response_type is "token", uncomment the next line will make it be "code".
  // auth.useCodeGrantFlow();
  return auth;
}

function signin_click(e: any) {
  auth.getSession();
}

function signout_click(e: any) {
  auth.signOut(); 
}

export default class CognitoAuthComponent extends Component<any, any> {
   constructor(props: any) {
    super(props);
  }
  
  componentDidMount() {
    let auth = initCognitoSDK();
    var curUrl = window.location.href;
    auth.parseCognitoWebResponse(curUrl);
  }
  render() {
    return (
      <div className="CognitoAuth">
        {(this.props.authState !== 'signedIn') ? 'not signed in' : 'signed in'}
        <div>
          <button onClick={signin_click}>SignIn</button> <button onClick={signout_click}>SignOut</button>

        </div>
      </div>
    );
  }
}
