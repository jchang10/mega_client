import * as React from 'react';
import { Component } from "react";
import "./Login.css";
import '../config';

import Amplify, {Auth, API} from 'aws-amplify';
import aws_exports from '../aws-exports';
import AWS from '../../node_modules/aws-sdk';

Amplify.configure(aws_exports);
// AWS.config.logger = console;
// window.LOG_LEVEL = 'DEBUG';

const AlwaysOn = (props:any) => {
  return (
      <div>
          <div>I am always here to show current auth state: {props.authState}</div>
          <button onClick={() => props.onStateChange('signUp')}>Show Sign Up</button>
      </div>
  )
}

function do_api_ping() {
  let apiName = 'myApiName';
  let path = '/path';
  let options = {
  }
  //JAE official API method. does not work for us, unless we fully adopt
  //the AWS way of doing things. ie. using awsmobile-cli for everything.
  return API.get('mega-project-test','/api/v1/users/ping',options)
 }

function do_api_status(token:string) {
  let apiName = 'myApiName';
  let path = '/path';
  let options = {
    headers:{Authorization: 'Bearer '+token}
  }
  return API.get('mega-project-test','/api/v1/auth/status',options)
 }

export default class Test extends Component<any,any> {
   constructor(props:any) {
    super(props);
    this.state = {
      token: props.authData.signInUserSession ? props.authData.signInUserSession.idToken : 'No User Session',
      do_api_status_response: "",
      email: "",
      password: ""
    };

  }
  async componentDidMount() {
    if (this.props.authState == 'signedIn' && this.props.authData.signInUserSession) {
      let token = this.props.authData.signInUserSession.idToken.jwtToken;
      let response_status = await do_api_status(token);
      this.setState({do_api_status_response:response_status});
      let response_ping = await do_api_ping();
      console.log('do_api_ping_response:'+JSON.stringify(response_ping));
    }
  }
  render() { 
    return (
      <div className="Test">
        <AlwaysOn {...this.props} />
        {(this.props.authState !== 'signedIn') ? 'not signed in' : 'signed in'}
        <div>
          this.state.token: <br/> <textarea rows={20} cols={100} value={JSON.stringify(this.state.token, null, 4)} readOnly />
        </div>
        <div>
          this.state.do_api_status response: <br/> <textarea rows={20} cols={100} value={JSON.stringify(this.state.do_api_status_response,null,4)} readOnly />
        </div>
      </div>
    );
  }
}
