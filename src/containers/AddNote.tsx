// import './AddNote.css';


import * as React from 'react';
import { graphql, compose, OperationOption, ChildProps } from "react-apollo";
import { Mutation } from 'react-apollo'
import gql from "graphql-tag";

import { NOTES_QUERY } from '../containers/Notes';

interface InputProps {
  userId?: string
  key?: string
  value?: string
}
interface State {
  userId?: string
  key?: string
  value?: string
}
class AddNote extends React.Component<InputProps, State> {
 
  state = {
    userId: '',
    key: '',
    value: '',
  }
  
  render() {
    const { userId, key, value } = this.props
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-md-3">
            userid: <input id="userId" type="text" value={this.state.userId} onChange={this.handleChange} />
          </div>
          <div className="col-md-3">
            key: <input id="key" type="text" value={this.state.key} onChange={this.handleChange} />
          </div>
          <div className="col-md-3">
            value: <input id="value" type="text" value={this.state.value} onChange={this.handleChange} />
          </div>
          <div className="col-md-3">
            <Mutation mutation={ADD_NOTES_MUTATION} update={this.handleAddCache}>
              {addNote => (
                <button 
                  className="ui positive button" 
                  onClick={(e) => this.handleAdd(e, addNote)}>Add</button>
              )}
            </Mutation>
            <Mutation mutation={UPDATE_NOTES_MUTATION}>
              {updateNote => (
                <button 
                  className="ui positive button" 
                  onClick={(e) => this.handleUpdate(e, updateNote)}>Update</button>
              )}
            </Mutation>
            <Mutation mutation={DELETE_NOTES_MUTATION}>
              {(deleteNote, {loading}) => (
                <span>
                {loading ? 'loading...' : (<button 
                  className="ui positive button"
                  onClick={(e) => this.handleDelete(e, deleteNote)}>Delete</button>)}
                </span>
              )}
            </Mutation>
          </div>
        </div>
      </React.Fragment>
    );
  }

  handleAdd = async (e, addNote) => {
    // e.stopPropagation();
    // e.preventDefault();
    const { userId, key, value } = this.state
    const newnote: Note = {userId,key,value};
    addNote({
      variables:newnote,
      // refetchQueries: [{ query: NOTES_QUERY }],
      // /* HACK AASContext:{doIt:true} needed to get refetchQueries to work past AppSync */
      // context: { AASContext: { doIt: true }}
    })
  }

  handleAddCache = (cache, {data:{addNote}}) => {
    const { notes } = cache.readQuery({query:NOTES_QUERY})

    cache.writeQuery({
      query: NOTES_QUERY,
      data: {
        notes: notes.concat(addNote.note)
      }
    })    
  }

  handleUpdate = async (e, updateNote) => {
    const { userId, key, value} = this.state
    const note:Note = {userId,key,value}
    updateNote({variables:note})
  }

  handleDelete = async (e, deleteNote) => {
    const { userId, key, value } = this.state
    // if (window.confirm(`Are you sure you want to delete userId=${userId}`)) {
      const response = await deleteNote({
        variables:{userId},
        refetchQueries: [{ query: NOTES_QUERY }],
        /* HACK AASContext:{doIt:true} needed to get refetchQueries to work past AppSync */
        context: { AASContext: { doIt: true } }
      })
      // }
  }

  handleChange = (event) => {
    this.setState({ [event.target.id]: event.target.value });
  }

}

const ADD_NOTES_MUTATION = gql`
mutation MyAddNote($userId:String, $key:String, $value:String){
  addNote(input:{userId:$userId,key:$key,value:$value}){
    note {
      id
      userId
      note1
      note2
      note3
    }
  }
}`

const UPDATE_NOTES_MUTATION = gql`
mutation MyUpdateNote($userId:String, $key:String, $value:String){
  updateNote(input:{userId:$userId,key:$key,value:$value}){
    note {
      id
      userId
      note1
      note2
      note3
    }
  }
}`

const DELETE_NOTES_MUTATION = gql`
mutation MyDeleteNote($userId:String){
  deleteNote(input:{userId:$userId}){
    note {
      id
      userId
      note1
      note2
      note3
    }
  }
}`

type Note = {
  userId: string
  key: string
  value: string
}

type Response = {
  userId: string
  note1: string
  note2: string
  note3: string
};

type Variables = {
  note: Note
}

const _addNotesOptions: OperationOption<InputProps, Response> = {
  props: (props) => ({
    addNote: (note: Note) => props.mutate!({
      variables: note,
      refetchQueries: [{ query: NOTES_QUERY }],
      /* HACK AASContext:{doIt:true} needed to get refetchQueries to work past AppSync */
      context: { AASContext: { doIt: true } }
    }) as any,
  })
} as any

const _updateNotesOptions = {
  props: (props) => ({
    updateNote: (note: Note) => props.mutate!({
      variables: note,
    }),
  })
} as any

const _deleteNotesOptions: OperationOption<InputProps, Response> = {
  props: (props) => ({
    deleteNote: (userId) => props.mutate!(({
      variables: { userId },
      refetchQueries: [{ query: NOTES_QUERY }],
      context: { AASContext: { doIt: true } }
    }) as any),
  })
} as any

export default AddNote

// helpers