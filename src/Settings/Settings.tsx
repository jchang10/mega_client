import * as React from 'react';
import { RouteProps } from "react-router";
import appSyncConfig from '../AppSync';
import Amplify from 'aws-amplify';
import {API, graphqlOperation} from 'aws-amplify'
import aws_exports from '../aws-exports';

// let myAppConfig = {
//   // ...
//   'aws_appsync_graphqlEndpoint': 'https://rmim4jmzzrbqhkmbfupnzbhasm.appsync-api.us-west-2.amazonaws.com/graphql',
//   'aws_appsync_region': 'us-west-2',
//   'aws_appsync_authenticationType': 'AMAZON_COGNITO_USER_POOLS', // You have configured Auth with Amazon Cognito User Pool ID and Web Client Id
//   // ...
// }
// Amplify.configure(myAppConfig);

const QUERY_FACES = `query {
  faces {
    uuid
    name
  }
}`

let counter = 1

interface SettingsProps extends RouteProps {

}
interface SettingsState {
  credentials: any,
  jwtToken: any,
  faces: any
}
class Settings extends React.Component<SettingsProps, SettingsState> {
  state = {
    credentials: null,
    jwtToken: null,
    faces: []
  }

  async componentDidMount() {
    const credentials = await Amplify.Auth.currentCredentials()
    this.setState({credentials})
    try {
      let currentSession = await Amplify.Auth.currentSession()
      const jwtToken = ( await Amplify.Auth.currentSession() ).getIdToken().getJwtToken()
      this.setState({jwtToken})
    } catch(err) {
      console.log('Settings:componentDidMount:ERROR: '+err)
    }
  }

  render() {
    return (
      <div style={{textAlign:'left'}}>
        <ul>
          <li> Version: {process.env.REACT_APP_VERSION} </li>
          <li> AppSync: </li>
            <ul>
              <li> url: {appSyncConfig.graphqlEndpoint} </li>
              <li> region: {appSyncConfig.region} </li>
              <li> credentials: 
                <textarea 
                  rows={10} 
                  cols={100} 
                  value={JSON.stringify(this.state.credentials, null, 2)}
                />
              </li>
              <li> jwtToken: 
                <textarea 
                  rows={10} 
                  cols={100} 
                  value={JSON.stringify(this.state.jwtToken, null, 2)}
                />
              </li>
              <li> appsync: event:
                <textarea
                  rows={10}
                  cols={100}
                  value={JSON.stringify(this.state.faces)}
                />
                <button
                  children={'QUERY'}
                  onClick={ async () => {
                    const faces = await API.graphql(graphqlOperation(QUERY_FACES))
                    this.setState({faces})
                  }}
                />
              </li>
            </ul>
        </ul>
      </div>
    );
  }
}

export default Settings;
