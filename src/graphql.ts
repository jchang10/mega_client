

/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: MyUpdateFace
// ====================================================

export interface MyUpdateFace_updateFace_face {
  name: string;  // name
  path: string;  // path
  uuid: string;  // uuid
}

export interface MyUpdateFace_updateFace {
  face: MyUpdateFace_updateFace_face | null;
}

export interface MyUpdateFace {
  updateFace: MyUpdateFace_updateFace | null;
}

export interface MyUpdateFaceVariables {
  uuid: string;
  name?: string | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: MyDeleteFace
// ====================================================

export interface MyDeleteFace_deleteFace_face {
  name: string;  // name
}

export interface MyDeleteFace_deleteFace {
  face: MyDeleteFace_deleteFace_face | null;
}

export interface MyDeleteFace {
  deleteFace: MyDeleteFace_deleteFace | null;
}

export interface MyDeleteFaceVariables {
  faceId: string;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MyFace
// ====================================================

export interface MyFace_face_parts {
  id: string;                 // The ID of the object.
  __typename: "Parts";
  uuid: string;               // uuid
  part: string | null;        // part
  path: string;               // path
  size: number;               // size
  faceId: string;             // faceId
  gender: string | null;      // gender
  createDate: string | null;  // createDate
  updateDate: string | null;  // updateDate
  version: number;            // version
  etag: string | null;        // etag
}

export interface MyFace_face {
  id: string;                         // The ID of the object.
  __typename: "Faces";
  uuid: string;                       // uuid
  name: string;                       // name
  owner: string | null;               // owner
  path: string;                       // path
  createDate: string | null;          // createDate
  updateDate: string | null;          // updateDate
  version: number;                    // version
  parts: MyFace_face_parts[] | null;  // List of Parts for this FaceType
}

export interface MyFace {
  face: MyFace_face;
}

export interface MyFaceVariables {
  faceId?: string | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MyFaces
// ====================================================

export interface MyFaces_faces {
  id: string;                 // The ID of the object.
  __typename: "Faces";
  uuid: string;               // uuid
  name: string;               // name
  owner: string | null;       // owner
  path: string;               // path
  createDate: string | null;  // createDate
  updateDate: string | null;  // updateDate
  version: number;            // version
}

export interface MyFaces {
  faces: MyFaces_faces[];
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateFace
// ====================================================

export interface UpdateFace_updateFace_face {
  name: string;  // name
  path: string;  // path
  uuid: string;  // uuid
}

export interface UpdateFace_updateFace {
  face: UpdateFace_updateFace_face | null;
}

export interface UpdateFace {
  updateFace: UpdateFace_updateFace | null;
}

export interface UpdateFaceVariables {
  uuid: string;
  name?: string | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteFace
// ====================================================

export interface DeleteFace_deleteFace_face {
  name: string;  // name
}

export interface DeleteFace_deleteFace {
  face: DeleteFace_deleteFace_face | null;
}

export interface DeleteFace {
  deleteFace: DeleteFace_deleteFace | null;
}

export interface DeleteFaceVariables {
  faceId: string;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Face
// ====================================================

export interface Face_face_parts {
  id: string;                 // The ID of the object.
  __typename: "Parts";
  uuid: string;               // uuid
  part: string | null;        // part
  path: string;               // path
  size: number;               // size
  faceId: string;             // faceId
  gender: string | null;      // gender
  createDate: string | null;  // createDate
  updateDate: string | null;  // updateDate
  version: number;            // version
}

export interface Face_face {
  id: string;                       // The ID of the object.
  __typename: "Faces";
  uuid: string;                     // uuid
  name: string;                     // name
  owner: string | null;             // owner
  path: string;                     // path
  createDate: string | null;        // createDate
  updateDate: string | null;        // updateDate
  version: number;                  // version
  parts: Face_face_parts[] | null;  // List of Parts for this FaceType
}

export interface Face {
  face: Face_face;
}

export interface FaceVariables {
  faceId?: string | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Faces
// ====================================================

export interface Faces_faces {
  id: string;                 // The ID of the object.
  __typename: "Faces";
  uuid: string;               // uuid
  name: string;               // name
  owner: string | null;       // owner
  path: string;               // path
  createDate: string | null;  // createDate
  updateDate: string | null;  // updateDate
  version: number;            // version
}

export interface Faces {
  faces: Faces_faces[];
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: ProcessS3Face
// ====================================================

export interface ProcessS3Face_processS3Face_face_parts {
  s3Key: string | null;
}

export interface ProcessS3Face_processS3Face_face {
  name: string;                                            // name
  path: string;                                            // path
  parts: ProcessS3Face_processS3Face_face_parts[] | null;  // List of Parts for this FaceType
}

export interface ProcessS3Face_processS3Face {
  face: ProcessS3Face_processS3Face_face | null;
}

export interface ProcessS3Face {
  processS3Face: ProcessS3Face_processS3Face | null;
}

export interface ProcessS3FaceVariables {
  bucket: string;
  prefix: string;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: ProcessS3Faces
// ====================================================

export interface ProcessS3Faces_processS3Faces_faces_parts {
  s3Key: string | null;
}

export interface ProcessS3Faces_processS3Faces_faces {
  name: string;                                               // name
  path: string;                                               // path
  parts: ProcessS3Faces_processS3Faces_faces_parts[] | null;  // List of Parts for this FaceType
}

export interface ProcessS3Faces_processS3Faces {
  faces: ProcessS3Faces_processS3Faces_faces[] | null;
}

export interface ProcessS3Faces {
  processS3Faces: ProcessS3Faces_processS3Faces | null;
}

export interface ProcessS3FacesVariables {
  bucket: string;
  prefix: string;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: s3PartObjects
// ====================================================

export interface s3PartObjects_s3PartObjects_part {
  updateDate: string | null;  // updateDate
  createDate: string | null;  // createDate
  size: number;               // size
}

export interface s3PartObjects_s3PartObjects {
  key: string | null;
  size: number | null;
  lastModified: any | null;
  version: number | null;
  status: string | null;
  part: s3PartObjects_s3PartObjects_part | null;
}

export interface s3PartObjects {
  s3PartObjects: s3PartObjects_s3PartObjects[] | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CopyS3Objects
// ====================================================

export interface CopyS3Objects_copyS3Objects {
  errors: (string | null)[] | null;
}

export interface CopyS3Objects {
  copyS3Objects: CopyS3Objects_copyS3Objects | null;
}

export interface CopyS3ObjectsVariables {
  bucket: string;
  prefix: string;
  paths: string[];
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: MyAddNote
// ====================================================

export interface MyAddNote_addNote_note {
  id: string;            // The ID of the object.
  userId: string;        // user_id
  note1: string | null;  // note1
  note2: string | null;  // note2
  note3: string | null;  // note3
}

export interface MyAddNote_addNote {
  note: MyAddNote_addNote_note | null;
}

export interface MyAddNote {
  addNote: MyAddNote_addNote | null;
}

export interface MyAddNoteVariables {
  userId?: string | null;
  key?: string | null;
  value?: string | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: MyUpdateNote
// ====================================================

export interface MyUpdateNote_updateNote_note {
  id: string;            // The ID of the object.
  userId: string;        // user_id
  note1: string | null;  // note1
  note2: string | null;  // note2
  note3: string | null;  // note3
}

export interface MyUpdateNote_updateNote {
  note: MyUpdateNote_updateNote_note | null;
}

export interface MyUpdateNote {
  updateNote: MyUpdateNote_updateNote | null;
}

export interface MyUpdateNoteVariables {
  userId?: string | null;
  key?: string | null;
  value?: string | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: MyDeleteNote
// ====================================================

export interface MyDeleteNote_deleteNote_note {
  id: string;            // The ID of the object.
  userId: string;        // user_id
  note1: string | null;  // note1
  note2: string | null;  // note2
  note3: string | null;  // note3
}

export interface MyDeleteNote_deleteNote {
  note: MyDeleteNote_deleteNote_note | null;
}

export interface MyDeleteNote {
  deleteNote: MyDeleteNote_deleteNote | null;
}

export interface MyDeleteNoteVariables {
  userId?: string | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: AddNote
// ====================================================

export interface AddNote_addNote_note {
  id: string;            // The ID of the object.
  userId: string;        // user_id
  note1: string | null;  // note1
  note2: string | null;  // note2
  note3: string | null;  // note3
}

export interface AddNote_addNote {
  note: AddNote_addNote_note | null;
}

export interface AddNote {
  addNote: AddNote_addNote | null;
}

export interface AddNoteVariables {
  userId?: string | null;
  key?: string | null;
  value?: string | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateNote
// ====================================================

export interface UpdateNote_updateNote_note {
  id: string;            // The ID of the object.
  userId: string;        // user_id
  note1: string | null;  // note1
  note2: string | null;  // note2
  note3: string | null;  // note3
}

export interface UpdateNote_updateNote {
  note: UpdateNote_updateNote_note | null;
}

export interface UpdateNote {
  updateNote: UpdateNote_updateNote | null;
}

export interface UpdateNoteVariables {
  userId?: string | null;
  key?: string | null;
  value?: string | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteNote
// ====================================================

export interface DeleteNote_deleteNote_note {
  id: string;            // The ID of the object.
  userId: string;        // user_id
  note1: string | null;  // note1
  note2: string | null;  // note2
  note3: string | null;  // note3
}

export interface DeleteNote_deleteNote {
  note: DeleteNote_deleteNote_note | null;
}

export interface DeleteNote {
  deleteNote: DeleteNote_deleteNote | null;
}

export interface DeleteNoteVariables {
  userId?: string | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Notes
// ====================================================

export interface Notes_notes {
  id: string;            // The ID of the object.
  userId: string;        // user_id
  note1: string | null;  // note1
  note2: string | null;  // note2
  note3: string | null;  // note3
}

export interface Notes {
  notes: (Notes_notes | null)[] | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: NotesQuery
// ====================================================

export interface NotesQuery_notes {
  id: string;            // The ID of the object.
  userId: string;        // user_id
  note1: string | null;  // note1
  note2: string | null;  // note2
  note3: string | null;  // note3
}

export interface NotesQuery {
  notes: (NotesQuery_notes | null)[] | null;
}


/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: UsersQuery
// ====================================================

export interface UsersQuery_users {
  username: string | null;
  email: string | null;
  enabled: boolean | null;
  createDate: any | null;
}

export interface UsersQuery {
  users: (UsersQuery_users | null)[] | null;
}

/* tslint:disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================