import React, { Component } from 'react'
import { Dropdown, Menu } from 'semantic-ui-react'
import RouteNavItem from '../components/RouteNavItem'
import { Link } from 'react-router-dom'

export default class AppMenu extends Component {
  state = { activeItem: '' }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { activeItem } = this.state

    return (
      <Menu secondary>
        <Menu.Item>
            <Menu.Header name='TestApp' as={Link} to='/'>TestApp</Menu.Header>
        </Menu.Item>
        <Menu.Menu position='right'>
            <Dropdown item text='Facemaker'>
                <Dropdown.Menu>
                    <Dropdown.Item as={Link} to='/faces'>
                        Faces
                    </Dropdown.Item>
                    <Dropdown.Item as={Link} to='/public'>
                        Public
                    </Dropdown.Item>
                    <Dropdown.Item as={Link} to='/private'>
                        Private
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
            <Dropdown item text='Testing'>
                <Dropdown.Menu>
                    <Dropdown.Item as='a' href='https://withmelabs-faces.s3.us-west-2.amazonaws.com/testing/index.html#FaceAssets/'>
                        AWS S3 Explorer
                    </Dropdown.Item>
                    <Dropdown.Item as={Link} to='/faces.appsync'>
                        Faces AppSync
                    </Dropdown.Item>
                    <Dropdown.Item as={Link} to='/notes'>
                        Notes
                    </Dropdown.Item>
                    <Dropdown.Item as={Link} to='/notes.appsync'>
                        Notes AppSync
                    </Dropdown.Item>
                    <Dropdown.Item as={Link} to='/users'>
                        Users
                    </Dropdown.Item>
                    <Dropdown.Item as={Link} to='/test'>
                        Test
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
            <Menu.Item name='account' active={activeItem === 'account'} onClick={this.handleItemClick} />
            <Menu.Item name='settings' as={Link} to='/settings' />
        </Menu.Menu>
      </Menu>
    )
  }
}
