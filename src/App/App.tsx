import * as React from 'react';
import { Route, Switch, Link } from 'react-router-dom';
// import { Nav, Navbar, NavDropdown, MenuItem } from 'react-bootstrap';
// import logo from '../logo.svg';
import './App.css';

import Amplify from 'aws-amplify';
import aws_exports from '../aws-exports';
import { Authenticator, withAuthenticator } from 'aws-amplify-react';

import { ApolloProvider } from "react-apollo";
import AWSAppSyncClient from "aws-appsync";
import { Rehydrated } from "aws-appsync-react";
import appSyncConfig from '../AppSync';

import RouteNavItem from '../components/RouteNavItem';
import CognitoAuthComponent from '../containers/Cognito/CognitoAuth';
import CognitoAuthRedirect from '../containers/Cognito/Redirect';

import AppMenu from './AppMenu'
import Home from '../containers/Home';
import Login from '../containers/Login';
import Test from '../containers/Test';
import Upload from '../Faces/Upload';
import Users from '../containers/Users';
import Notes from '../containers/Notes';
import NotesAppSync from '../containers/NotesAppSync';
import Faces from '../Faces/Faces';
import FacesAppSync from '../Faces/FacesAppSync';
import S3List from '../Faces/S3List';
import Settings from '../Settings/Settings'

// AWS.config.logger = console;
(window as any).LOG_LEVEL = 'DEBUG';
Amplify.Logger

class App extends React.Component<any> {
  render() {
    if (this.props.authState == 'signedIn') {
      return (
        <div className="App container">
          <AppMenu />
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/login" exact component={Login} />
            <Route path='/test_page' exact render={(routeProps) => <Test {...this.props} {...routeProps} />} />
            <Route path='/users' exact render={(routeProps) => <Users {...this.props} {...routeProps} />} />
            <Route path='/faces' exact render={(routeProps) => <Faces {...this.props} {...routeProps} />} />
            <Route path='/faces.appsync' exact render={(routeProps) => <FacesAppSync {...this.props} {...routeProps} />} />
            <Route path='/public' exact render={(routeProps) => <S3List {...this.props} {...routeProps} />} />
            <Route path='/private' exact render={(routeProps) => <Upload {...this.props} {...routeProps} />} />
            <Route path='/settings' exact render={(routeProps) => <Settings {...this.props} {...routeProps} />} />
            <Route path='/notes' exact render={(routeProps) => <Notes {...this.props} {...routeProps} />} />
            <Route path='/notes.appsync' exact render={(routeProps) => <NotesAppSync {...this.props} {...routeProps} />} />
          </Switch>
        </div>
      );
    } else {
      return null
    }
  }
}

const client = new AWSAppSyncClient({
  url: appSyncConfig.graphqlEndpoint,
  region: appSyncConfig.region,
  auth: {
    type: appSyncConfig.authenticationType,
    // apiKey: appSyncConfig.apiKey,

    //
    // Commenting out all the rest of the auth config, authentication
    // happens automatically. The appropriate UserPool or Federated login works
    // correctly automatically.
    //

    // type: AUTH_TYPE.AWS_IAM,
    // Note - Testing purposes only
    /*credentials: new AWS.Credentials({
        accessKeyId: AWS_ACCESS_KEY_ID,
        secretAccessKey: AWS_SECRET_ACCESS_KEY
    })*/

    // Amazon Cognito Federated Identities using AWS Amplify
    // credentials: async () => await Amplify.Auth.currentCredentials(),

    // Amazon Cognito user pools using AWS Amplify
    // type: AUTH_TYPE.AMAZON_COGNITO_USER_POOLS,
    // jwtToken: async () => ( await Amplify.Auth.currentSession() ).getIdToken().getJwtToken()
  },
  disableOffline: true
});

// const AppWithAuth = withAuthenticator(App, { includeGreetings:true });
const federated = {
  google_client_id:'583774673754-ftnmq5qk1sfkbsv2de0noe6gu5fcp1an.apps.googleusercontent.com',
  // facebook_app_id:'1920589807968753' // MegaProject
  facebook_app_id:'201588443910663' // MegaProject - Test
}

const WithProvider = (props) => (
  <ApolloProvider client={client}>
    {/* Removed use of <Rehydrated> after latest update. Not sure exactly why it was needed. */}
    {/* <Rehydrated> */}
        <App {...props} />
    {/* </Rehydrated> */}
  </ApolloProvider>
);

export default withAuthenticator(WithProvider, {includeGreetings:true});

