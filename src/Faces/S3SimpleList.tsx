//// Does a simple Amplify.Storage list call and returns the result.

import * as path from 'path'
import * as React from 'react';
import Amplify, { Auth, Storage, Analytics } from 'aws-amplify';
import FetchMore from '../components/FetchMore'

import './S3SimpleList.css'

interface S3SimpleListProps {
    level:string
}
interface S3SimpleListState {
    s3objects: any
}
class S3SimpleList extends React.Component<S3SimpleListProps, S3SimpleListState> {

    state = {
        s3objects: undefined
    }

    // hard-coded
    readonly PREFIX = 'FaceAssets'

    async get_prefix(level='private') {
        // await Auth.currentSesion doesnt work as expected
        // await Auth.currentSession()
        await Amplify.Storage._ensureCredentials()
        const options = Amplify.Storage._options
        const prefix = Amplify.Storage._prefix({...options, level:level})
        return `${prefix}${this.PREFIX}`
    }

    get_s3() {
        return Amplify.Storage._createS3(Amplify.Storage._options)
    }
    
    async query_s3(cursor=null, count=20) {
        const prefix = await this.get_prefix()
        const options = Amplify.Storage._options
        const s3 = this.get_s3()
        var params = {
            Bucket: options.bucket,
            Prefix: prefix,
            MaxKeys: count,
            ContinuationToken: cursor
        }
        let data, data2
        try {
            data = await s3.listObjectsV2(params).promise()
        } catch (err) {
            console.log(err, err.stack)
            return
        }
        return data

        //// original version calling Storage.list. however, does not support paging.
        // const s3objects = await Storage.list(prefix, {level:level})
        // const objects = [
        //   {key:'one'},
        //   {key:'one/two/four/three/1.fbx'},
        //   {key:'one/three/three/2.fbx'},
        //   {key:'one/three/five/3.fbx'},
        //   {key:'one/four/five/4.fbx'}
        // ]
        // this.setState({s3objects})

        // s3.listObjectsV2(params, function(err, data) {
        //      if (err) console.log(err, err.stack); // an error occurred
        //      else     console.log(data);           // successful response
        //      console.log('TEST2: '+s3)
        //      console.log('TEST3: '+params)

        //      /*
        //      data = {
        //       Contents: [
        //          {
        //         ETag: "\"70ee1738b6b21e2c8a43f3a5ab0eee71\"", 
        //         Key: "example1.jpg", 
        //         LastModified: <Date Representation>, 
        //         Owner: {
        //          DisplayName: "myname", 
        //          ID: "12345example25102679df27bb0ae12b3f85be6f290b936c4393484be31bebcc"
        //         }, 
        //         Size: 11, 
        //         StorageClass: "STANDARD"
        //        }, 
        //          {
        //         ETag: "\"9c8af9a76df052144598c115ef33e511\"", 
        //         Key: "example2.jpg", 
        //         LastModified: <Date Representation>, 
        //         Owner: {
        //          DisplayName: "myname", 
        //          ID: "12345example25102679df27bb0ae12b3f85be6f290b936c4393484be31bebcc"
        //         }, 
        //         Size: 713193, 
        //         StorageClass: "STANDARD"
        //        }
        //       ], 
        //       NextMarker: "eyJNYXJrZXIiOiBudWxsLCAiYm90b190cnVuY2F0ZV9hbW91bnQiOiAyfQ=="
        //      }
        //      */
        //    });
    }

    async componentDidMount() {
        console.debug('componentDidMount...')
        const data = await this.query_s3()
        this.setState({s3objects:data})
    }

    onFetchMore = async ({variables:{cursor}, updateQuery}) => {
        const data = await this.query_s3(cursor)
        const {s3objects:prev} : {s3objects:any} = this.state
        this.setState({
            s3objects:{
                ...prev,
                ...data,
                Contents: [
                    ...prev.Contents,
                    ...data.Contents
                ]
            }
        })
    }

    /** Strings prefix from s3key and returns face specific path. */
    static face_path(key:string):string {
        return key.split(path.sep).slice(2).join(path.sep)
    }

    render() {
        const {s3objects} : {s3objects:any} = this.state
        return (
            <div className='S3SimpleList'>
                <ul>
                    {s3objects && s3objects.Contents.map(obj => (
                        <li key={obj.Key}> {S3SimpleList.face_path(obj.Key)} </li>
                    ))} 
                </ul>           
                <FetchMore
                    loading={false}
                    hasNextPage={s3objects && (s3objects as any).IsTruncated || false }
                    variables={{cursor: s3objects && s3objects.NextContinuationToken || null}}
                    updateQuery={undefined}
                    fetchMore={this.onFetchMore}
                    children={'files...'}
                />
            </div>
        )
    }
}

export default S3SimpleList