import * as path from 'path'
import * as React from 'react';
import Amplify, { Auth, Storage, Analytics } from 'aws-amplify';
import { S3Album } from 'aws-amplify-react'
import aws_exports from '../aws-exports';
import { Grid, Header, List, Segment, Table, Icon,
         Form, Button, Popup } from 'semantic-ui-react'
import { Accordion, AccordionTitleProps, Divider, SemanticCOLORS } from 'semantic-ui-react'
import gql from "graphql-tag";
import { Query, Mutation, MutationUpdaterFn, MutationFn, MutationOptions } from 'react-apollo';
import Moment from 'react-moment'

import { FacePart, FaceAsset } from '../Faces/Faces'
import SearchBar from '../components/SearchBar';

import * as graphql from '../graphql'
import './S3List.css'
// import PopupContent from '../../patches/semantic-ui-react/src/modules/Popup/PopupContent';

// Amplify.configure(aws_exports);
// Amplify.Logger.LOG_LEVEL = 'DEBUG'

// const BUCKET = Amplify.Storage.vault._options.bucket
const BUCKET = 'withmelabs-faces'
const PREFIX = 'FaceAssets/Facemaker/v8'

type MyItem = {
  files: string[],
  dirs: string[]
}
type MyItems = {
  [key:string] : MyItem 
}
// type ProcessS3FaceMutationFunc = MutationFn<S3ProcessFaceMutationData>
// interface S3FaceMutationFunc {
//   (MutationFn, string):void
// }

interface MyListItemProps {
  s3keys:MyItems,
  mykey:string,
  level:number,
  handleListItemOnClick:Function,
  // or another way:
  // s3PartObjects:{[key:string]:S3PartObject}
  s3PartObjects:Map<string,S3PartObject>,
  handleProcessingOnClick:(processS3Face:ProcessS3FaceMutationFunc, key:string) => any,
  handleProcessingAllOnClick:(processS3Faces:ProcessS3FacesMutationFunc, key:string) => any,
  handleProcessingUpdate?:MutationUpdaterFn,
  handleAccordionRowClick:(event: React.MouseEvent<HTMLDivElement>, data: AccordionTitleProps) => void,
  activeIndex:string,
}
const MyListItem = (props:MyListItemProps) => {
    const {s3keys, mykey, level, s3PartObjects, activeIndex} = props
    const s3partobject = s3PartObjects[mykey]
    const {version=null, status=null, lastModified=null, part=null} = s3partobject || {}
    const s3itemobj = s3keys[mykey]

    const list_item = (
      <List.Item
        onClick={props.handleListItemOnClick(mykey)} 
        key={mykey}
        >{[
            //
            // if on level1, then assume these are the FaceAssets. thus, display
            // the "processing" button so a user can process the face_assets
            //
            <List.Content floated='right' key={mykey+'2'}>
              {level>0 ? (
                <S3ProcessFaceMutation 
                  mutation={S3PROCESSFACE_MUTATION}
                  update={props.handleProcessingUpdate}>
                  {(processS3Face, {loading}) => {
                    if (s3itemobj) {
                      return <Button 
                                color={loading ? 'blue' : 'green'}
                                size='tiny' compact
                                onClick={props.handleProcessingOnClick(processS3Face, mykey)}
                                >
                                {loading ? 'Processing...' : 'Process'}
                              </Button>
                    } else {
                      return null
                    }
                  }}
                </S3ProcessFaceMutation>
              ) : (
                <S3ProcessFacesMutation 
                  mutation={S3PROCESSFACES_MUTATION}
                  update={props.handleProcessingUpdate}>
                  {(processS3Faces, {loading}) => {
                    if (s3itemobj) {
                      return <Button 
                                color={loading ? 'blue' : 'green'}
                                size='tiny' compact
                                onClick={props.handleProcessingAllOnClick(processS3Faces, mykey)}
                                >
                                {loading ? 'Processing...' : 'Process All Faces'}
                              </Button>
                    } else {
                      return null
                    }
                  }}
                </S3ProcessFacesMutation>
              )}

            </List.Content>,
            //
            // if s3file exists in dynamodb, then display the processing button
            //
            <List.Content floated='right' key={mykey+'1'}>
              {version ? (
                <Popup trigger={<span>v{version} &nbsp;</span>}>
                  database last updated <Moment fromNow>{part?part.updateDate:part.createDate}</Moment>
                </Popup>
              ) : ''}
              {s3itemobj ? null : (
                <Popup
                  trigger={
                    <Button color={(() => {
                        let color
                        if (status == 'added')
                          color = 'green'
                        if (status == 'new')
                          color = 'blue'
                        if (status == 'modified')
                          color = 'yellow'
                        if (status == 'deleted')
                          color = 'red'
                        return color as SemanticCOLORS
                      })()} size='tiny' compact>{status}</Button>
                    }>
                    file last modified <Moment fromNow>{lastModified}</Moment>
                </Popup>
              )}
            </List.Content>,

          //
          // for all s3file's, display the name, at least here.
          //
          level == 0 && s3itemobj == undefined ? (
            <div key={mykey+'3'}>Not found</div> 
          ) : (
            <List.Content key={mykey+'3'}>
              {"\u00a0".repeat(level*4)} <Icon name={s3itemobj ? 'folder' : 'file'} />&nbsp;{path.basename(mykey)}
            </List.Content>
          )

          // end of the list
      ]}</List.Item>
    )

    // LIST_DIRS
    let list_dirs:any = null
    if (s3itemobj && s3itemobj.dirs.length) {
      list_dirs = s3itemobj.dirs.map(key => 
        <div key={key} >
          <Divider
            fitted // no space
            />
          <MyListItem
            {...props}
            mykey={mykey+'/'+key}
            key={mykey+'/'+key}
            level={level+1}
            />
        </div>
      )
    }
    // LIST FILES
    let list_files:any = null
    if (s3itemobj && s3itemobj.files.length) {
      list_files = s3itemobj.files.map(key => 
        <div key={key} >
          <Divider
            fitted // no space
            />
          <MyListItem
            {...props}
            mykey={mykey+'/'+key} 
            key={mykey+'/'+key}
            level={level+1}
            />
        </div>
      )
    }

    if (level == 1) {
      return (
        <React.Fragment>
          <Accordion.Title 
              index={mykey}
              active={activeIndex==mykey}
              onClick={props.handleAccordionRowClick}
            >
            <React.Fragment>{list_item}</React.Fragment>
          </Accordion.Title>
          <Accordion.Content
              index={mykey}
              active={activeIndex==mykey}
              >
            <React.Fragment>{[list_dirs, list_files]}</React.Fragment>
          </Accordion.Content>
        </React.Fragment>
      )
    } else {
      return <React.Fragment>{[list_item, list_dirs, list_files]}</React.Fragment>
    }
}

const S3PROCESSFACE_MUTATION = gql`
  mutation ProcessS3Face($bucket:String!, $prefix:String!) {
    processS3Face(input:{bucket:$bucket,prefix:$prefix}) {
      face {
        name 
        path
        parts {
          s3Key
        }
      }
    }
  }
  `
type ProcessS3FaceMutationFunc = MutationFn<graphql.ProcessS3Face,graphql.ProcessS3FaceVariables>
//// Using the apollo-gen created schema declaration file
//// % apollo-codegen generate **/*.graphql --schema schema.json --target typescript --output graphql.ts
// interface ProcessS3FaceMutation {
//   processS3Face : {
//     face: FaceAsset
//   }
// }
class S3ProcessFaceMutation extends Mutation<graphql.ProcessS3Face> {}

const S3PROCESSFACES_MUTATION = gql`
  mutation ProcessS3Faces($bucket:String!, $prefix:String!) {
    processS3Faces(input:{bucket:$bucket,prefix:$prefix}) {
      faces {
        name 
        path
        parts {
          s3Key
        }
      }
    }
  }
  `
type ProcessS3FacesMutationFunc = MutationFn<graphql.ProcessS3Faces,graphql.ProcessS3FacesVariables>
class S3ProcessFacesMutation extends Mutation<graphql.ProcessS3Faces> {}


interface S3ListProps {

}
interface S3ListState {
  objects: any[],
  s3keys: object,
  // or another way:
  // s3faces:{[key:string]:S3FaceObject}
  s3faces: Map<string, S3PartObject>,
  rootText: string,
  activeIndex: string,
}
class S3List extends React.Component<S3ListProps, S3ListState> {
  _keys:MyItems = {}
  _initialized = false
  state = { 
    objects: [],
    s3keys: {},
    // or another way:
    // s3faces: {}
    s3faces: new Map<string,S3PartObject>(),
    rootText : PREFIX,
    activeIndex : '-1',
  }
  /** process all keys into dirs and files hierarchy.
   */
  _add_key(key:string) {
    let dirname = path.dirname(key)
    let basename = path.basename(key)
    if (dirname == '.')
      return
    // process files
    if (this._keys[dirname]) {
      if (!this._keys[dirname].files.includes(basename))
        this._keys[dirname].files.push(basename)
    } else {
      this._keys[dirname] = {files:[basename], dirs:[]}
    }
    // process folders
    while (dirname != '.') {
      basename = path.basename(dirname)
      dirname = path.dirname(dirname)
      if (dirname == '.')
        break
      if (this._keys[dirname]) {
        if (this._keys[dirname].dirs.includes(basename) == false)
          this._keys[dirname].dirs.push(basename)
      } else {
        this._keys[dirname] = {files:[], dirs:[basename]}
      }
    }
  }
  async componentDidMount() {
    console.debug('componentDidMount...')
    /* ignore
    const objects = await Storage.list('FaceAssets', {level:'public'})
    // const objects = [
    //   {key:'one'},
    //   {key:'one/two/four/three/1.fbx'},
    //   {key:'one/three/three/2.fbx'},
    //   {key:'one/three/five/3.fbx'},
    //   {key:'one/four/five/4.fbx'}
    // ]
    objects.forEach(object => this._add_key(object.key))
    for (var key in this._keys) {
      console.log('key: '+key+' value: '+this._keys[key])
    }
    this.setState({keys:this._keys})
    */
    // Object.assign(this.state, {s3faces:this._s3faces, s3keys:this._keys})
    
  }
  handleListItemOnClick = mykey => e => {
    // e.stopPropagation()
    console.debug(`handleListItemOnClick root:${e} key:${mykey}`)
  }
  handleRootTextChange = e => {
    console.debug('handleRootTextChange...')
    if (e.key === 'Enter') {
      this.setState({rootText:e.target.value})
    }
   }
  handlePrefixChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    this.setState({rootText:e.target.value})
  }
  handleAccordionRowClick = (e, titleProps) => {
    const { index } = titleProps
    const { activeIndex } = this.state
    const newIndex = activeIndex === index ? -1 : index
    console.log('handleAccordionRowClick... newIndex:'+newIndex)
    this.setState({ activeIndex: newIndex })
  }
  render() {
    return (
      <S3ObjectsQuery query={QUERY_OBJECTS}>
        {({ loading, error, data }) => {
          if (loading) return "Loading...";
          if (error) return `Error! ${error.message}`;
          
          const {s3PartObjects} = data!

          if (this._initialized == false) {
            const objects = s3PartObjects
            objects.forEach(object => this._add_key(object.key))
            for (var key in this._keys) {
              console.debug('S3List render key: '+key+' value: '+this._keys[key])
            }
            
            // convert array into object hash and assign to a copy of the object.
            // cannot use original response object, which is read-only by apollo.
            // thus, make a copy here and use that instead.
            // const s3faces = Object.assign({}, ...objects.map(obj => ({[obj.key]:Object.assign({},obj)})))
            // slightly better than above...
            // cannot do: {...objects.map(...)} however... due to object spread differences
            const s3faces = Object.assign({}, ...objects.map(obj => ( {[obj.key]:{...obj}} ) ) )

            // s3keys represents S3 files
            // s3faces represents dynamodb data
            Object.assign(this.state, {s3faces, s3keys:this._keys})

            this._initialized = true
          }

          return (
            <div className="S3List">
              <h2> Public Bucket </h2>

              Prefix: &nbsp;
                <select name='Prefixes' onChange={this.handlePrefixChange}>
                  <option value=''> Select Root Prefix </option>
                  <option> {PREFIX} </option>
                </select>
                {/* <input onKeyPress={this.handleRootTextChange} /> */}
                <Form.Input 
                  style={{width:"500px"}}
                  onKeyPress={this.handleRootTextChange}
                  defaultValue={PREFIX} />

              <Accordion>
                <List 
                  selection 
                  divided 
                  verticalAlign='middle'
                  size='small'
                  style={{textAlign:'left'}}
                  >
                  <MyListItem 
                    s3keys={this.state.s3keys}
                    mykey={this.state.rootText}
                    level={0}
                    handleListItemOnClick={this.handleListItemOnClick}
                    s3PartObjects={this.state.s3faces}
                    handleProcessingOnClick={this.handleProcessingOnClick}
                    handleProcessingAllOnClick={this.handleProcessingAllOnClick}
                    // handleProcessingUpdate={this.handleProcessingUpdate}
                    handleAccordionRowClick={this.handleAccordionRowClick}
                    activeIndex={this.state.activeIndex}
                    />
                </List>
              </Accordion>
            </div>
          )
        }}
      </S3ObjectsQuery>
    )
  }
  handleProcessingAllOnClick = (processS3Faces:ProcessS3FacesMutationFunc, key:string) => async (e:React.MouseEvent<HTMLDivElement>,props) => {
    let {s3faces} = this.state
    console.debug('handleProcessingAllOnClick key:',key)
    e.stopPropagation()
    const input = {bucket:BUCKET, prefix:key}
    const response = await processS3Faces({variables: input})
    const data = response && response.data && response.data.processS3Faces
    data && data.faces!.map(face => {
      face.parts!.map(part => {
        if (part) {
          let {s3Key} = part
          s3Key = String(s3Key) //typing
          if (s3faces[s3Key]) {
            let {status, version} = s3faces[s3Key]
            s3faces[s3Key] = { ...s3faces[s3Key],
              status:'added', 
            }
          }
        }
      })
    })
    this.setState({s3faces:s3faces})
  }
  handleProcessingOnClick = (processS3Face:ProcessS3FaceMutationFunc, key:string) => async (e:React.MouseEvent<HTMLDivElement>,props) => {
    let {s3faces} = this.state
    console.debug('handleProcessingOnClick key:',key)
    e.stopPropagation()
    const input = {bucket:BUCKET, prefix:key}
    const response = await processS3Face({variables: input})
    const data = response && response.data && response.data.processS3Face
    data && data.face!.parts!.map(part => {
      if (part) {
        let {s3Key} = part
        s3Key = String(s3Key) //typing
        if (s3faces[s3Key]) {
          let {status, version} = s3faces[s3Key]
          s3faces[s3Key] = { ...s3faces[s3Key],
            status:'added', 
          }
        }
      }
    })
    this.setState({s3faces:s3faces})
  }
  handleProcessingUpdate = (cache, {data:{processS3Face}}) => {
    // Using update method from apollo's in-memory cache.
    // This method also works but is a bit more tedious to use, i think.
    // The cache is an array of objects and not a map. thus, you have to
    // scan the array to find the object you want.
    const { s3PartObjects:s3faces } = cache.readQuery({query:QUERY_OBJECTS})
    console.debug('handleProcessingUpdate...')
    processS3Face.face.parts.map(part => {
      const {s3Key} = part
      let obj = s3faces.find(face => face.key == s3Key)
      if (obj) {
        obj.status = 'added'
        console.debug('handleProcessingUpdate... found')
      }
    })

    cache.writeQuery({
      query: QUERY_OBJECTS,
      data: {
        s3PartObjects: s3faces
      }
    })    
  }
}

export const QUERY_OBJECTS = gql`
  query s3PartObjects {
    s3PartObjects(bucket:"${BUCKET}", prefix:"${PREFIX}") {
      key
      size
      lastModified
      version
      status
      part {
        updateDate
        createDate
        size
      }
    }
  }
`;

type S3PartObject = {
  key: string,
  size: number,
  lastModified: Date,
  version: number,
  status: string,
}
interface QueryObjectsData {
  s3PartObjects: S3PartObject[]
}
class S3ObjectsQuery extends Query<QueryObjectsData> {}


export default S3List