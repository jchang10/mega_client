import * as path from 'path'
import * as React from 'react';
import ReactDOM from 'react-dom'
import Amplify from 'aws-amplify';
import aws_exports from '../aws-exports';
import AWS from 'aws-sdk';
import Dropzone, { DropFilesEventHandler } from 'react-dropzone'
import { Table, Button, Checkbox, Icon, Popup } from 'semantic-ui-react'
import gql from "graphql-tag";
import { GraphqlApi } from 'aws-sdk/clients/appsync';
import { Mutation, MutationUpdaterFn, MutationFn, MutationOptions } from 'react-apollo';
import * as graphql from '../graphql'
import S3SimpleList from './S3SimpleList'

import './Upload.css'
// import { Popup } from '../../patches/semantic-ui-react/dist/es';

Amplify.configure(aws_exports);
// window.LOG_LEVEL = 'DEBUG';

const BUCKET = Amplify.Storage._options.bucket
const PREFIX = 'FaceAssets'

function __guardMethod__(obj, methodName, transform) {
  if (typeof obj !== 'undefined' && obj !== null && typeof obj[methodName] === 'function') {
    return transform(obj, methodName);
  } else {
    return undefined;
  }
}

type MyFile = File & { fullPath:string, status?:string, notchecked?:boolean }

interface S3State {
  files: MyFile[],
  allchecked: boolean,
  deletePrivateBucket: boolean,
}
export default class S3 extends React.Component<any,S3State> {
  s3objects = {}
  options = {
    ignoreHiddenFiles: true
  }
  files = [] as MyFile[]

  state = { 
    files: [] as MyFile[],
    allchecked: true,
    deletePrivateBucket: false,
  }

  async componentDidMount() {
    console.debug('componentDidMount...')
    const list = await Amplify.Storage.list()
    list.map(obj => this.s3objects[obj.key] = obj)
  }

  handleDrop:DropFilesEventHandler = (files, rejected, event) => {
    // files.map((file) => {
    //   Storage.put(file.name, file, {level:'private'});
    // });
    this.files.length = 0
    this._drop(event)
  }

  _drop(e) {
    let result
    if (!e.dataTransfer) {
      return;
    }
    // this.emit("drop", e);

    // Convert the FileList to an Array
    // This is necessary for IE11
    let files = [] as MyFile[]
    for (let i = 0; i < e.dataTransfer.files.length; i++) {
      files[i] = e.dataTransfer.files[i];
    }

    // this.emit("addedfiles", files);

    // Even if it's a folder, files.length will contain the folders.
    if (files.length) {
      let {items} = e.dataTransfer;
      if (items && items.length && (items[0].webkitGetAsEntry != null)) {
        // The browser supports dropping of folders, so handle items instead of files
        result = this._addFilesFromItems(items);
      } else {
        result = this.handleFiles(files);
      }
    }
    console.debug(`_drop: files: ${this.files.length} result: ${result.length}`)
  }

  handleFiles(files) {
    console.debug('handleFiles...')
    // Only handle a folder
    // for(let file of files) {
    //   this.addFile(file);
    // }
  }

  addFile(file) {
    // Compare this file with the one in the private folder
    const key = path.join(PREFIX, file.fullPath)
    if (this.s3objects[key]) {
      const privdate = this.s3objects[key].lastModified
      const localdate = file.lastModifiedDate
      if (privdate < localdate) {
          file.status = `newer - ${localdate} vs ${privdate}`
      }
    } else {
      file.status = 'new'
    }

    this.files.push(file)
    this.setState({files:this.files})

    return file
  }

  // When a folder is dropped (or files are pasted), items must be handled
  // instead of files.
  // on the initial call, level==0
  _addFilesFromItems(items, level=0) {
    // return (() => {
      let result = [] as any[];
      for (let item of items) {
        var entry;
        if ((item.webkitGetAsEntry != null) && (entry = item.webkitGetAsEntry())) {
          if (level == 0 && entry.isFile) {
            console.debug('ignoring files. drop a folder instead')
          } else if (entry.isFile) {
            console.debug('file:')
            result.push(this.addFile(item.getAsFile()));
          } else if (entry.isDirectory) {
            console.debug('dir: '+entry.name)
            // Append all files from that directory to files
            result.push(this._addFilesFromDirectory(entry, entry.name, level+1));
          } else {
            result.push(undefined);
          }
        } else if (item.getAsFile != null) {
          if ((item.kind == null) || (item.kind === "file")) {
            result.push(this.addFile(item.getAsFile()));
          } else {
            result.push(undefined);
          }
        } else {
          result.push(undefined);
        }
      }
      return result;
    // })();
  }

  // Goes through the directory, and adds each file it finds recursively
  // initial call level=0
  _addFilesFromDirectory(directory, path, level:number) {
    let dirReader = directory.createReader();

    let errorHandler = error => __guardMethod__(console, 'log', o => o.log(error));

    var readEntries = async () => {
      return dirReader.readEntries(entries => {
            if (entries.length > 0) {
              for (let entry of entries) {
                if (entry.isFile) {
                  entry.file(file => {
                    if (this.options.ignoreHiddenFiles && (file.name.substring(0, 1) === '.')) {
                      return;
                    }
                    file.fullPath = `${path}/${file.name}`;
                    return this.addFile(file);
                  });
                } else if (entry.isDirectory) {
                  this._addFilesFromDirectory(entry, `${path}/${entry.name}`, level+1);
                }
              }

              // Recursively call readEntries() again, since browser only handle
              // the first 100 entries.
              // See: https://developer.mozilla.org/en-US/docs/Web/API/DirectoryReader#readEntries
              readEntries();
            }
            return null;
          }
          , errorHandler);
    };

    readEntries();
    console.debug('_addFilesFromDirectory DONE files:'+this.files.length)
  }

  // hard-coded
  readonly PREFIX = 'FaceAssets'

  handleUploadClick = (event, data) => {
    const {files} = this.state
    console.debug('handleUploadClick...')
    this.set_files_status('Uploading...')
    files.filter(file => !file.notchecked)
      .map(async file => {
        let response = await Amplify.Storage.put(`${this.PREFIX}/${file.fullPath}`, file, {level:'private'});
        file.status = 'Uploaded'
        this.setState({files})
      })
  }

  // Adds our own custom prefix to the AWS prefix
  // e.g. aws prefix: "private/us-west-2:7365534e-f3c5-4076-a8db-9e3360caac99/"
  //      our prefix: "FaceAssets"
  // returns "private/us-west-2:7365534e-f3c5-4076-a8db-9e3360caac99/FaceAssets"
  async get_prefix() {
    // await Auth.currentSesion doesnt work as expected
    // await Auth.currentSession()
    await Amplify.Storage._ensureCredentials()
    const options = Amplify.Storage._options
    const prefix = Amplify.Storage._prefix({...options, level:'private'})
    return `${prefix}${this.PREFIX}`
  }

  set_files_status(status:string) {
    const {files} = this.state
    files.filter(file => !file.notchecked)
          .map(file => file.status = status)
    this.setState({files})
  }

  handleSubmitClick = (copyS3Objects:CopyS3ObjectsMutationFunc) => async (event, props) => {
    const {files} = this.state
    console.debug('handleSubmitClick...')
    const paths = files.filter(file => !file.notchecked)
                        .map(file => file.fullPath)
    const prefix = await this.get_prefix()
    const input = {
      bucket: BUCKET, // e.g. "testwebapp-userfiles-mobilehub-1585053506"
      prefix: prefix, // e.g. "private/us-west-2:7365534e-f3c5-4076-a8db-9e3360caac99/FaceAssets"
      paths: paths // e.g. "Facemaker/male/eye/eye.FBX"
    }
    //// MAIN COPY
    this.set_files_status('copying...')
    const response = await copyS3Objects({variables: input})
    //// MAIN COPY
    const data = response && response.data && response.data.copyS3Objects
    const errors = data && data.errors
    files.filter(file => !file.notchecked)
          .map(file => {
            if (errors && errors.includes(file.fullPath)) {
              file.notchecked = true
              file.status = 'copy failed'
            } else {
              file.status = 'copied'
            }
            this.setState({files})
          })
  }

  render() {
    const {files, allchecked} = this.state
    return (
      <section className="Upload">
        <div className="dropzone">
          <Dropzone 
            onDrop={this.handleDrop}
            multiple
            >
            <p>Drop a Face Asset Root Folder here</p>
          </Dropzone>
        </div>
        <div>
          {files.length ? (
            <React.Fragment>
              <Button 
                color='green'
                onClick={this.handleUploadClick}
                > 1. Upload to Private Folder </Button> 
              <Icon name='angle double right' />
              <CopyS3ObjectsMutation mutation={COPYS3OBJECTS_MUTATION}>
                {(copyS3Objects, {loading}) => {
                  if (loading) 
                    return <Button color='red'
                              active={false}
                            > Working... </Button>
                  else      
                    return <Button color='green'
                              onClick={this.handleSubmitClick(copyS3Objects)}
                            > 2. Submit to Public Folder </Button>
                }}
              </CopyS3ObjectsMutation>
            </React.Fragment>
          ) : ( null )}
        </div>
        <aside>
          <h2>{files.length ? 'Dropped files. Total count:' +files.length : null }</h2>
          <Table celled compact><Table.Body>
            { this.state.files.length ? (
              <tr>
                <th></th>
                <th> <Checkbox key='all' fitted checked={allchecked} onClick={(e) => {
                    files.map(f => f.notchecked = allchecked)
                    this.setState({files, allchecked:!allchecked})
                  }} /> </th>
                <th>Directory</th>
                <th>Name</th>
                <th>Size (bytes)</th>
                <th>Status</th>
              </tr>
             ) : null }
            { this.state.files.map((f) => 
                <tr key={f.fullPath}>
                  <td></td>
                  <td> <Checkbox key={f.fullPath} checked={!f.notchecked} onClick={(e) => {
                      f.notchecked = !f.notchecked
                      this.setState({files})
                    }} /> </td>
                  <td>{path.dirname(f.fullPath)}</td>
                  <td>{f.name}</td>
                  <td>{f.size}</td>
                  <td>{f.status}</td>
              </tr>) }
          </Table.Body></Table>
        </aside>
        <aside>
          <span className="Header"> Private Bucket </span>
            <Popup on='click' hideOnScroll
              open={this.state.deletePrivateBucket}
              onOpen={() => this.setState({deletePrivateBucket:true})}
              onClose={() => this.setState({deletePrivateBucket:false})}
              trigger={
                <Button size='tiny' compact>Delete All</Button>
              }
            >
              <Popup.Content>
                Confirm deleting all files in Private Bucket?
                <Button size='tiny' compact
                  onClick={this.handleDeletePrivateBucketButtonClick}
                >YES</Button>
              </Popup.Content>
            </Popup>
            <S3SimpleList level='private' {...this.state.deletePrivateBucket} />
        </aside>
      </section>
    );
  }
  handleDeletePrivateBucketButtonClick = async (e:React.MouseEvent<any>) => {
    console.log('handleDeletePrivateBucketButtonClick...')
    const list = await Amplify.Storage.list(PREFIX, {level:'private'})
    list.map(obj => {
      console.log(`removing... ${obj.key}`)
      Amplify.Storage.remove(obj.key, {level:'private'})
    })
    console.log('done removing')
    this.setState({files:[]})
    this.setState({deletePrivateBucket:false})
  }
}

const COPYS3OBJECTS_MUTATION = gql`
mutation CopyS3Objects($bucket:String!, $prefix:String!, $paths:[String!]!) {
  copyS3Objects(input:{bucket:$bucket, prefix:$prefix, paths:$paths}) {
    errors
  }
}
`
type CopyS3ObjectsMutationFunc = MutationFn<graphql.CopyS3Objects, graphql.CopyS3ObjectsVariables>
class CopyS3ObjectsMutation extends Mutation<graphql.CopyS3Objects> {}
