import * as React from "react";
import gql from "graphql-tag";
import { Query, Mutation, ApolloConsumer, MutationFn } from 'react-apollo';
import { RouteProps } from "react-router";

// import { Grid, Row, Col } from 'react-bootstrap'
import { Grid, Header, List, Segment, Table, Accordion, Icon, Label,
          Form, AccordionTitleProps, Popup} from 'semantic-ui-react'

import SearchBar from '../components/SearchBar'
import * as graphql from '../graphql'

import './Faces.css'

import Amplify from 'aws-amplify';
import { graphqlOperation } from 'aws-amplify'
import { Connect } from 'aws-amplify-react'

// AWS.config.logger = console;
// (window as any).LOG_LEVEL = 'DEBUG';

export type FacePart = {
  id: string,
  uuid: string,
  faceId: string,
  path: string,
  owner?: string,
  size: number,
  part: string | null,
  gender: string | null,
  createDate: Date | string | null,
  updateDate: Date | string | null,
  version: number,
  s3Key?: string | null,
  s3Bucket?: string | null,
  etag: string | null,
}
export type FaceAsset = {
  uuid: string,
  name: string,
  path: string,
  owner: string | null,
  parts?: (FacePart)[] | null,
}
interface FacesTableProps {
  faces: FaceAsset[],
  filterText: string
}
interface FacesTableState {
  activeIndex: any,
  editrow:string,
  parts: FacePart[],
}
class FacesTable extends React.Component<FacesTableProps, FacesTableState> {
  state = {
    activeIndex: '',
    editrow: '',
    parts: [],
  }

  handleAccordionClick = async (e:React.MouseEvent<HTMLDivElement>, 
                                            titleProps:AccordionTitleProps) => {
    const { index } = titleProps
    const { activeIndex } = this.state
    const newIndex = activeIndex === index ? '-1' : index
    console.debug(`FacesTable handleAccordionClick:${index}`)
    this.setState({
      activeIndex: newIndex,
      editrow: '',
    })
    //region unused. keeping for future reference.
    // if (newIndex != '-1') {
    //   // const {data} = await client.query({
    //   const response = await client.query({
    //     query: FACE_QUERY,
    //     variables:{faceId:newIndex}
    //   })
    // }
    // const test= 'test'
    //endregion
  }
  handleRowEditOnClick = face_id => (e:React.MouseEvent<any>) => {
    console.debug('FacesTable handleRowEditOnClick:'+face_id)
    e.stopPropagation()
    if (this.state.editrow == face_id)
      this.setState({editrow:''})
    else
      this.setState({editrow:face_id})
  }
  handleFaceNameOnClick = face_id => (e:React.MouseEvent<any>) => {
    console.debug('FacesTable handleFaceNameOnClick:'+face_id)
    e.stopPropagation()
  }
  handleFaceNameTextChange = (updateFace:MyUpdateFaceMutationFunc, face:FaceAsset) => (e:React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      // this.setState({rootText:e.target.value})
      console.debug('handleFaceNameTextChange...'+e.currentTarget.value)
      const newname = e.currentTarget.value
      if (newname != face.name) {
        console.debug(`new name is "${newname}"`)
        const newface = {...face, name:newname}
        updateFace({
          variables:newface,
          update:this.handleFaceNameTextChangeUpdate,
        })
        // this.setState({editrow:''})
      }
    }
  }
  handleFaceNameTextChangeUpdate = (cache, {data:{updateFace}}) => {
    let {faces} = cache.readQuery({query:FACES_QUERY})
    faces = faces.map(face => {
      if (face.uuid == updateFace.face.uuid)
        face = {...face, ...updateFace.face}
      return face
    })
    cache.writeQuery({query: FACES_QUERY, data:{faces}})
    this.setState({editrow:''})
  }
  render() {
    console.debug('FacesTable render...')
    const { activeIndex } = this.state
    const {faces, filterText} = this.props;
    return (
      <Accordion fluid styled
        style={{textAlign:'left'}}
        as={Table}
        selectable
        unstackable
        ><Table.Body>
          <Table.Row>
            <Table.HeaderCell> Face Name </Table.HeaderCell>
            <Table.HeaderCell> Face Path </Table.HeaderCell>
          </Table.Row>
          {(
            faces.filter(face => filterText == '' ||
                          activeIndex as any != '-1' ||
                          face.name.includes(filterText))
            .map((face, i) => [
                    <Accordion.Title
                      active={activeIndex==face.uuid}
                      index={face.uuid}
                      onClick={this.handleAccordionClick}
                      as={Table.Row}
                      key={face.uuid+'title'}
                      >
                        <Table.Cell collapsing>
                          { this.state.editrow == face.uuid ?
                              <Mutation mutation={UPDATE_FACE_MUTATION}>
                                {(updateFace, {loading}) => (loading) ? (
                                    'Updating...'
                                  ) : (
                                    <Form.Input 
                                      style={{width:'90%'}}
                                      label={<Icon name='dropdown' />} 
                                      defaultValue={face.name}
                                      onClick={this.handleFaceNameOnClick(face.uuid)} 
                                      onKeyPress={this.handleFaceNameTextChange(updateFace, face)}
                                      />
                                  )
                                }
                              </Mutation> 
                              :
                              [<Icon key='icon' name='dropdown' />, face.name] 
                          }
                        </Table.Cell>
                        <Table.Cell collapsing>
                              {face.path}
                        </Table.Cell>
                        { activeIndex == face.uuid ?
                            <Table.Cell width={1} singleLine textAlign="right">
                              {(() => {
                                if (this.state.editrow != face.uuid)
                                  return (
                                    <Label onClick={this.handleRowEditOnClick(face.uuid)}>
                                      <Icon name='edit'/> Edit Options
                                    </Label>
                                  )
                                else {
                                  return (
                                    <React.Fragment>
                                      <Mutation mutation={DELETE_FACE_MUTATION}>
                                        {(deleteFace, {loading}) => {
                                          if (loading) return 'Deleting...'
                                          return (
                                            <Label onClick={this.handleDeleteFaceOnClick(deleteFace, face.uuid)}>
                                              <Icon name='delete'/> Delete
                                            </Label>
                                          )
                                        }}
                                      </Mutation>
                                      <Label onClick={this.handleRowEditOnClick(face.uuid)}>
                                        <Icon name='user cancel'/> User Cancel
                                      </Label>
                                    </React.Fragment>
                                  )
                                }
                              })()}
                            </Table.Cell> : undefined}
                    </Accordion.Title>,
              <Accordion.Content
                active={activeIndex==face.uuid}
                as={Table.Row}
                key={face.uuid+'content'}
                >
                  <Table.Cell colSpan={3}>
                    <PartsTable
                      activeIndex={activeIndex} 
                      face={face}
                      filterText={filterText}
                      />
                  </Table.Cell>
              </Accordion.Content>
          ])
        )}
      </Table.Body></Accordion>
    )
  }

  handleDeleteFaceOnClick = (deleteFace, face_id:string) => async (e:React.MouseEvent<any>, props) => {
    if (confirm('delete face? '+face_id)) {
      e.stopPropagation()
      const response = await deleteFace({
        variables:{faceId:face_id},
        refetchQueries: [{ query: FACES_QUERY }],
        /* HACK AASContext:{doIt:true} needed to get refetchQueries to work past AppSync */
        context: { AASContext: { doIt: true } }
      })
    }
  }
}

// Example of a SFC.
// Different ways to type SFC's. Below uses destructuring...
// const PartsTable = ({
//   activeIndex,
//   face,
//   filterText,
// } : {
//   activeIndex:string,
//   face:FaceAsset,
//   filterText:string,
// }) => (
interface PartsTableProps {
  activeIndex:string,
  face:FaceAsset,
  filterText:string,
}
const PartsTable = ({activeIndex, face, filterText}:PartsTableProps) =>
  <Table unstackable size='small'>
    <Table.Body>
      <Table.Row>
        <Table.HeaderCell width={1}>  </Table.HeaderCell>
        <Table.HeaderCell> Part       </Table.HeaderCell>
        <Table.HeaderCell> Property   </Table.HeaderCell>
        <Table.HeaderCell> Value      </Table.HeaderCell>
        <Table.HeaderCell width={1}>  </Table.HeaderCell>
      </Table.Row>
      { activeIndex == face.uuid ? 
        <FaceQuery query={FACE_QUERY} variables={{faceId:face.uuid}}>
          { ({loading, error, data, client}) => {
            if (loading) return <tr><td>"Loading..."</td></tr>
            if (error) return <tr><td>`Error! ${error.message}`</td></tr>
            if (data && data.face && data.face.parts) {
              // const dataface:FaceAsset = data.face
              // const datapart:(FacePart|null)[] = data.face.parts
              return <PartsRows
                        face={data.face}
                        filterText={filterText}
                        />
            } else 
              return undefined
          }}
        </FaceQuery> : undefined
      }
    </Table.Body>
  </Table>

interface PartsRowsProps {
  face: FaceAsset,
  filterText: string,
}
class PartsRows extends React.Component<PartsRowsProps> {
  state = {
    editrows:{},
    activeIndex:'',
  }
  handleEditRowClick = row_id => {
    console.log('handleOnClick '+row_id)
    if (this.state.editrows[row_id]) {
      delete this.state.editrows[row_id]
    } else {
      this.state.editrows[row_id] = true
    }
    this.setState({
      editrows:this.state.editrows
    })
  }
  handleAccordionClick = (e, titleProps) => {
    const { index } = titleProps
    const { activeIndex } = this.state
    const newIndex = activeIndex === index ? -1 : index

    this.setState({ activeIndex: newIndex })
  }
  confirmDelete = (e) => confirm('Delete this part?')
  render() {
    const {activeIndex} = this.state
    const {face, filterText} = this.props
    
    if (face.parts)
      return (
        face.parts.map(part => {
            let last_part:FacePart
            return Object.keys(part)
              .filter(k => k[0] && k[0].startsWith && k[0].startsWith('_') == false) //skip private
              .filter(k => k != 'faceId')
              .filter(k => filterText == '' || //match the filter
                (part[k] && part[k].includes && part[k].includes(filterText)))
              .map(k => {
                const row_id = part.uuid+'-'+k
                if (last_part != part) {
                  last_part = part
                  return [
                    <Table.Row key={row_id}>
                      <Table.Cell>
                        <Icon name='delete' onClick={this.confirmDelete} />
                      </Table.Cell>
                      <Table.Cell>{part.part}</Table.Cell>
                      <Table.Cell></Table.Cell>
                      <Table.Cell>{part.path}</Table.Cell>
                    </Table.Row>,
                    <PartRow
                      part={part}
                      k={k}
                      onEditRowClick={this.handleEditRowClick}
                      isEdit={this.state.editrows[row_id]}
                      key={row_id+'unique2'}
                      />
                  ]
                } else {
                  last_part = part
                  return <PartRow
                            part={part}
                            k={k}
                            onEditRowClick={this.handleEditRowClick}
                            isEdit={this.state.editrows[row_id]}
                            key={row_id+'unique3'}
                            />
                }
              })
            }
        )
      )
    else
      return 'Nothing'
  }
}

// Example of a SFC with a handler
interface PartRowProps {
  part: FacePart,
  k: string,
  onEditRowClick: any,
  isEdit: boolean,
}
const PartRow = ({part, k, onEditRowClick, isEdit}:PartRowProps) => {
  const handleOnClick = (row_id:string) => e => {
    onEditRowClick(row_id)
  }
  const row_id = part.uuid+'-'+k
  return isEdit ? 
      <Table.Row key={row_id}>
        <Table.Cell>
          <Icon name='user cancel' onClick={handleOnClick(row_id)} /> 
        </Table.Cell>
        <Table.Cell> </Table.Cell>
        <Table.Cell> 
          <Form.Input defaultValue={k} fluid /> 
        </Table.Cell>
        <Table.Cell>
          <Form.Input defaultValue={part[k]} fluid /> 
        </Table.Cell>
        <Table.Cell> 
          <Icon name='save' onClick={handleOnClick(row_id)} /> 
        </Table.Cell>
      </Table.Row>
    :
      <Table.Row key={row_id}>
        <Table.Cell>
          <Icon name='edit' onClick={handleOnClick(row_id)} /> 
        </Table.Cell>
        <Table.Cell> </Table.Cell>
        <Table.Cell>
          { k }
        </Table.Cell>
        <Table.Cell>
          { part[k] }
        </Table.Cell>
        <Table.Cell> 

        </Table.Cell>
      </Table.Row>
}

interface FilterableFacesTableProps {
  faces: FaceAsset[],
}
interface FilterableFacesTableState {
  filterText: string
}
class FilterableFacesTable extends React.Component<FilterableFacesTableProps, FilterableFacesTableState>
{
  state = {
    filterText:''
  }
  handleFilterTextChange = (filterText) => {
    this.setState({
      filterText: filterText
    });
  }
  render() {
    return (
      <div>
        <SearchBar
          filterText={this.state.filterText}
          onFilterTextChange={this.handleFilterTextChange}
        />
        &nbsp;
        <FacesTable
          faces={this.props.faces}
          filterText={this.state.filterText}
        />
      </div>
    );
  }
}

const UPDATE_FACE_MUTATION = gql`
mutation MyUpdateFace($uuid:ID!, $name:String) {
  updateFace(input:{uuid:$uuid, name:$name}) {
    face {
      name
      path
      uuid
    }
  }
}
`
type MyUpdateFaceMutationFunc = MutationFn<graphql.MyUpdateFace, graphql.MyUpdateFaceVariables>
const DELETE_FACE_MUTATION = gql`
  mutation MyDeleteFace($faceId:ID!) {
    deleteFace(input:{faceId:$faceId}) {
      face {
        name
      }
    }
  }
`
const FACE_QUERY = gql`
  query MyFace($faceId:ID) {
    face(faceId:$faceId) {
      id __typename uuid name owner path createDate updateDate version
      parts {
        id __typename uuid part path size faceId gender
        createDate updateDate version etag
      }
    }
  }
`
interface FaceData {
  face: FaceAsset
}
interface FaceProps {
  face: FaceAsset
}
class FaceQuery extends Query<graphql.MyFace> {}
//const FaceQueryWrapper = (props:FaceProps & RouteProps) => (
//FaceQueryWrapper unused but keeping here for reference
const FaceQueryWrapper = (props:any) => (  
  <FaceQuery 
    query={FACE_QUERY} 
    variables={{faceId:props.face.uuid}}
    >
    { ({loading, error, data, client}) => {
      if (loading) return "Loading..."
      if (error) return `Error! ${error.message}`
      if (data && data.face && data.face.parts)
        return <React.Fragment>
                {React.cloneElement(props.children, {...props, parts:data.face.parts})}
              </React.Fragment>
      else
        return undefined
    }}
  </FaceQuery>
)

export const FACES_QUERY = gql`
  query MyFaces {
    faces {
      id __typename uuid name owner path createDate updateDate version
    }
  }
`;
interface FacesQueryData {
  faces: FaceAsset[],
}
class FacesQuery extends Query<FacesQueryData> {
  
}
const Faces = (props:RouteProps) => (
  <FacesQuery query={FACES_QUERY}>
    {({ loading, error, data, client}) => {
        if (loading) return "Loading...";
        if (error) return `Error! ${error}`;
        const {faces} = data!
        return (
          <div className="Faces">
            <h2> Face Assets </h2>
            <FilterableFacesTable faces={faces} />
          </div>
        );
    }}
  </FacesQuery>
)

export default Faces

