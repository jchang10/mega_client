import * as React from "react";
import { Route, RouteComponentProps } from "react-router-dom";
import { Dropdown } from 'semantic-ui-react'

export default (props: any) => 
  <Route
    path={props.href}
    exact
    children={({ match, history }) =>
      <props.as
        as='div'
        onClick={(e: any) => history.push(e.currentTarget.getAttribute("href"))}
        {...props}
        active={match ? true : false}
      >
        {props.children}
      </props.as>}
  />;


