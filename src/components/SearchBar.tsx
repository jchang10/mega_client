import * as React from 'react'

import { Input } from 'semantic-ui-react'

interface SearchBarProps {
  filterText: string,
  onFilterTextChange?: any,
  onEnterKeyPress?: Function,
}
class SearchBar extends React.Component<SearchBarProps> {
  handleFilterTextChange = (e) => {
    if (this.props.onFilterTextChange)
      this.props.onFilterTextChange(e.target.value);
  }
  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      if (this.props.onEnterKeyPress)
        this.props.onEnterKeyPress(e.target.value)
    }
  }
  render() {
    return (
        <input
          type="text"
          placeholder="Search..."
          value={this.props.filterText}
          onChange={this.handleFilterTextChange}
          onKeyPress={this.handleKeyPress}
        />
    );
  }
}

export default SearchBar
